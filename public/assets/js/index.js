function init() {
    var user_email = '';
    /*var currentUser = firebase.auth().currentUser;
    currentUser.sendEmailVerification().then(function() {
        console.log("Account certification has sent!");
    }, function(error) {
        console.error("Account certification has error!");
    });*/
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='./index.html'><i class='fas fa-sign-out-alt'  id='logout_btn'></i></a>" + "<a href='userpage.html'>"+user.email+"</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout_btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    create_alert("success", "Bye!");
                }).catch(function(error){
                    create_alert("error", "Retry!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<li><a href='signin.html'><i class='fas fa-user-plus fa-2x' style='color:#FFFFFF;'></i></a></li><li><a href='signin.html'><i class='fas fa-sign-in-alt fa-2x' style='color:#FFFFFF;'></i></a></li>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var notification=document.getElementById('notification');
    notification.addEventListener('click', function(e){
        e.preventDefault();
        if(!window.Notification){
            console.log('Not supported');
        }
        else{
            Notification.requestPermission().then(function(p){
                if(p=='denied'){
                    console.log('You denied to show notification');
                }
                else if(p=='granted'){
                    console.log('You allowed to show notification');
                }
            })
        }
    })

}

window.onload = function () {
    init();
};