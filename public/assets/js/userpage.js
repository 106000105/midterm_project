var Girl = firebase.database().ref('Girl');
var Music = firebase.database().ref('Music');
var Makeup = firebase.database().ref('Makeup');
var Fasion = firebase.database().ref('Fasion')
var diy = firebase.database().ref('DIY');
var Foodie = firebase.database().ref('Foodie');
var Relationship = firebase.database().ref('Relationship');
var Travel = firebase.database().ref('Travel');
var Movie = firebase.database().ref('Movie');
var Meme = firebase.database().ref('Meme');
var Game = firebase.database().ref('Game');
var Boy = firebase.database().ref('Boy');
var Life = firebase.database().ref('Life');
var School = firebase.database().ref('School');
var Science = firebase.database().ref('Science');
var Animal = firebase.database().ref('Animal');

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='./index.html'><i class='fas fa-sign-out-alt'  id='logout_btn'></i></a>" + "<a href='userpage.html'>"+user.email+"</a>";
            document.getElementById('post_list').innerHTML = "<br>"+"My account: "+user.email+"<br><br>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout_btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    create_alert("success", "Bye!");
                }).catch(function(error){
                    create_alert("error", "Retry!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<li><a href='signin.html' class='join1'><i class='fas fa-user-plus fa-2x' style='color:#FFFFFF;'></i></a></li><li><a href='signin.html' class='join2'><i class='fas fa-sign-in-alt fa-2x' style='color:#FFFFFF;'></i></a></li>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    
    Girl.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeGirl('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Girl.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeGirl('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Music.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMusic('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Music.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMusic('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Makeup.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMakeup('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Makeup.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMakeup('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Fasion.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeFasion('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Fasion.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeFasion('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    diy.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeDIY('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            diy.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeDIY('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Foodie.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeFoodie('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Foodie.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeFoodie('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Relationship.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeRelationship('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Relationship.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeRelationship('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Travel.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeTravel('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Travel.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeTravel('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Movie.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMovie('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Movie.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMovie('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Meme.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMeme('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Meme.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeMeme('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Game.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeGame('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Game.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeGame('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Boy.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeBoy('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Boy.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeBoy('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Life.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeLife('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Life.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeLife('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    School.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeSchool('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            School.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeSchool('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Science.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeScience('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Science.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeScience('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));

    Animal.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                if(childsnapshot[key].email==user_email){
                    console.log('first');
                    total_post.push("<h2>" + childsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeAnimal('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                    document.getElementById('post_list').innerHTML = total_post.join();
                    console.log(total_post);
                }
            };
            Animal.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    if(newchildsnapshot[key].email==user_email){
                        console.log('third');
                        total_post.push("<h2>" + newchildsnapshot[key].title + "</h2>" + `<i class='fas fa-times' style='color: rgb(255, 51, 51, 0.4); margin-left: 95%' onclick="removeAnimal('` + key + `')"></i>` + "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></hr>");
                        document.getElementById('post_list').innerHTML = total_post.join();
                    }
                }
                
            });
        })
        .catch(e => console.log(e.message));
}

function removeGirl(k){
    firebase.database().ref('Girl/'+k).remove();
}
function removeMusic(k){
    firebase.database().ref('Music/'+k).remove();
}
function removeMakeup(k){
    firebase.database().ref('Makeup/'+k).remove();
}
function removeFasion(k){
    firebase.database().ref('Fasion/'+k).remove();
}
function removeDIY(k){
    firebase.database().ref('DIY/'+k).remove();
}
function removeFoodie(k){
    firebase.database().ref('Foodie/'+k).remove();
}
function removeRelationship(k){
    firebase.database().ref('Relationship/'+k).remove();
}
function removeTravel(k){
    firebase.database().ref('Travel/'+k).remove();
}
function removeMovie(k){
    firebase.database().ref('Movie/'+k).remove();
}
function removeMeme(k){
    firebase.database().ref('Meme/'+k).remove();
}
function removeGame(k){
    firebase.database().ref('Game/'+k).remove();
}
function removeBoy(k){
    firebase.database().ref('Boy/'+k).remove();
}
function removeLife(k){
    firebase.database().ref('Life/'+k).remove();
}
function removeSchool(k){
    firebase.database().ref('School/'+k).remove();
}
function removeScience(k){
    firebase.database().ref('Science/'+k).remove();
}
function removeAnimal(k){
    firebase.database().ref('Animal/'+k).remove();
}

function changePassword(){

    console.log("Reset PWD");
    var auth = firebase.auth();
    var emailAddress = auth.currentUser.email;

    auth.sendPasswordResetEmail(emailAddress).then(function() {
        console.log("You can check your email now, and the account will automatically logout " )

        firebase.auth().signOut()
        .then(function(result){
            window.location.assign("index.html");
        })
        .catch(function(error) {
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error", errorMessage);
        });

      // Email sent.
    }).catch(function(error) {
      // An error happened.
    });

}

window.onload = function () {
    init();
};