function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        
        /*var currentUser = firebase.auth().currentUser;
        console.log(currentUser.isEmailVerified());
        if(!currentUser.isEmailVerified()){
            currentUser.sendEmailVerification().then(function() {
            console.log("Account certification has sent!");
        }, function(error) {
            console.error("Account certification has error!");
        });
        }*/
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='./index.html'><i class='fas fa-sign-out-alt'  id='logout_btn'></i></a>" + "<a href='userpage.html'>"+user.email+"</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout_btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    create_alert("success", "Bye!");
                }).catch(function(error){
                    create_alert("error", "Retry!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<li><a href='signin.html'><i class='fas fa-user-plus fa-2x' style='color:#FFFFFF;'></i></a></li><li><a href='signin.html'><i class='fas fa-sign-in-alt fa-2x' style='color:#FFFFFF;'></i></a></li>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_tit = document.getElementById('title');
    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            firebase.database().ref('Makeup').push({
                // ...
                title:changeWord(post_tit.value),
                email:user_email,
                data:changeWord(post_txt.value)
            });
            /*postRef.push().set({
                // ...
                "email":{user.email},"data":{post_txt.value}
            });*/
            post_tit.value="";
            post_txt.value="";
        }
    });

    // The html code for post
    var str_first_title = "<h2>";
    var str_second_context = "</h1><table style='background-color: #FFFFFF'><tr><td><h1 style='width:365px; text-overflow: ellipsis; overflow:hidden;'>";
    var str_after_content = `</h1></td><td><button type='button' onclick="changeHTML('`;
    var str_end_content = `')">Learn more</button></td></tr></table><hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'>`;
    
    // var str_first_title2 = "<h2>";
    // var str_second_context2 = "</h1><table style='background-color: #FFFFFF'><tr><td><h1 style='width:365px; text-overflow: ellipsis; overflow:hidden;'>";
    // var str_after_content2 = "</h1></td><td><button type='button'>Learn more</button></td></tr></table><p style='color: rgb(144, 144, 144, 0.5)'>──────────────────────────────────────────────────────────────────────────────</p>";
    
    var postsRef = firebase.database().ref('Makeup');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var childsnapshot=snapshot.val();
            for(var key in childsnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                console.log('first');
                total_post.push(str_first_title + childsnapshot[key].title + "</h2><h1>" + childsnapshot[key].email + str_second_context + childsnapshot[key].data + str_after_content + key + str_end_content);
                document.getElementById('post_list').innerHTML = total_post.join();
                console.log(total_post);
            };
            postsRef.on('value', function(newsnapshot){
                newchildsnapshot=newsnapshot.val();
                total_post.length=0;
                for(var key in newchildsnapshot){
                    console.log('third');
                    total_post.push(str_first_title + newchildsnapshot[key].title + "</h2><h1>" + newchildsnapshot[key].email + str_second_context + newchildsnapshot[key].data + str_after_content + key + str_end_content);
                    document.getElementById('post_list').innerHTML = total_post.join();
                }

            });
        })
        .catch(e => console.log(e.message));
        /*postsRef.on('value', function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            //var abc = snapshot.val().data || 'Anonymous';
            total_post = [];
            snapshot.forEach(function (childSnapshot) {
                var value = childSnapshot.val();
                total_post.push(str_before_username + value.email + "</strong>" + value.data + str_after_content);
            });
            document.getElementById('post_list').innerHTML = total_post.join(' ');
        })*/

        if(isNotify){
            Notify();
        }
        else{
            isNotify=1;
        }
}

var isNotify=-1;
function Notify(){
    var notification=document.getElementById('notification');
    notification.addEventListener('click', function(e){
        e.preventDefault();
        if(!window.Notification){
            console.log('Not supported');
        }
        else{
            Notification.requestPermission().then(function(p){
                if(p=='denied'){
                    console.log('You denied to show notification');
                }
                else if(p=='granted'){
                    console.log('You allowed to show notification');
                }
            })
        }

    })

    var Makeup=firebase.database().ref('Makeup');
    Makeup.on('child_added', function(data){
        if(Notification.permission!='default'){
            var notify;
            notify=new Notification('New topic from Makeup', {
                'User: ':data.val().email,
                'shares: ':data.val().title,
                'says: ':data.val().data
            })
            
            notify.onclick=function(){
                console.log('onclick');
            }
        }
        else{
            console.log('Please allow the notification first');
        }
    });
}

var newchildsnapshot;
function changeHTML(n){
    console.log(newchildsnapshot[n]);
    window.location.href='section3_content.html?'+n;
}

function changeWord(val) {
    if(val === null) return '';
    val = val.replace(/&/g, "&amp;");
    val = val.replace(/\"/g, "&quot;");
    val = val.replace(/\'/g, "&#039;");
    val = val.replace(/</g, "&lt;");
    val = val.replace(/>/g, "&gt;");
    return val;
}
window.onload = function () {
    init();
};