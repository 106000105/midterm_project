var url = location.href;
var dataSent = url.split("?");
var article = dataSent[1];

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='./index.html'><i class='fas fa-sign-out-alt'  id='logout_btn'></i></a>" + "<a href='userpage.html'>"+user.email+"</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout_btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    create_alert("success", "Bye!");
                }).catch(function(error){
                    create_alert("error", "Retry!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<li><a href='signin.html'><i class='fas fa-user-plus fa-2x' style='color:#FFFFFF;'></i></a></li><li><a href='signin.html'><i class='fas fa-sign-in-alt fa-2x' style='color:#FFFFFF;'></i></a></li>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            firebase.database().ref('Makeup'+article.toString()).push({
                // ...
                email:user_email,
                data:post_txt.value
            });
            /*postRef.push().set({
                // ...
                "email":{user.email},"data":{post_txt.value}
            });*/
            post_txt.value="";
        }
    });

    // The html code for post
    var i=0;
    var j=0;
    var str_first_title = "<h2>";
    var str_second_context = "</h1><h1>";
    var str_after_content = "</h1>";
    var str_end_content = "<hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'>";
    
    var str_before_username = "<div><hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'><h2>Comment B";
    var str_after_content2 = "</h1><hr size='8px' align='center' width='100%' style='color: rgb(144, 144, 144, 0.5)'></div>";
    
    // var str_first_title2 = "<h2>";
    // var str_second_context2 = "</h1><table style='background-color: #FFFFFF'><tr><td><h1 style='width:365px; text-overflow: ellipsis; overflow:hidden;'>";
    // var str_after_content2 = "</h1></td><td><button type='button'>Learn more</button></td></tr></table><p style='color: rgb(144, 144, 144, 0.5)'>──────────────────────────────────────────────────────────────────────────────</p>";
    
    var postsRef = firebase.database().ref('Makeup/'+article.toString());
    var postsRef2 = firebase.database().ref('Makeup'+article.toString());
    
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    
    postsRef.once('value')
    .then(function (snapshot) {
        /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
        ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
        ///         2. Join all post in list to html in once
        ///         4. Add listener for update the new post
        ///         5. Push new post's html to a list
        ///         6. Re-join all post in list to html when update
        ///
        ///         Hint: When history post count is less then new post count, update the new and refresh html

            console.log('first');
            total_post.push(str_first_title + snapshot.val().title + "</h2><h1>" + snapshot.val().email + str_second_context + snapshot.val().data + str_after_content + str_end_content);
            document.getElementById('post_list').innerHTML = total_post.join();
            console.log(total_post);

            postsRef.on('value', function(newsnapshot){
                total_post.length=0;
                console.log('third');
                total_post.push(str_first_title + newsnapshot.val().title + "</h2><h1>" + newsnapshot.val().email + str_second_context + newsnapshot.val().data + str_after_content + str_end_content);
                document.getElementById('post_list').innerHTML = total_post.join();
                
            });
        })
        .catch(e => console.log(e.message));

    postsRef2.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childSnapshot) {
                //var childKey = childSnapshot.key;
                //var childData = childSnapshot.val();
                // ...
                console.log('second');
                total_post.push(str_before_username + i.toString() + "</h2><h1>" + childSnapshot.val().email + "<br>" + childSnapshot.val().data + str_after_content2);
                document.getElementById('comment_list').innerHTML = total_post.join();
                console.log(total_post);
                i++;
            });
            i=0;
            postsRef2.on('value', function(newsnapshot){
                total_post.length=0;
                newsnapshot.forEach(function(newchildSnapshot){
                    console.log('fourth');
                    total_post.push(str_before_username + j.toString() + "</h2><h1>" + newchildSnapshot.val().email + "<br>" + newchildSnapshot.val().data + str_after_content2);
                    document.getElementById('comment_list').innerHTML = total_post.join();
                    j++;
                })
                j=0;
            });
        })
        .catch(e => console.log(e.message));
        /*postsRef.on('value', function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            //var abc = snapshot.val().data || 'Anonymous';
            total_post = [];
            snapshot.forEach(function (childSnapshot) {
                var value = childSnapshot.val();
                total_post.push(str_before_username + value.email + "</strong>" + value.data + str_after_content);
            });
            document.getElementById('post_list').innerHTML = total_post.join(' ');
        })*/
}

window.onload = function () {
    init();
};